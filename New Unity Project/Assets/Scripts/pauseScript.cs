﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseScript : MonoBehaviour
{
    public GameObject Screen;
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown (KeyCode.Escape)) // Activate when the escape key is pressed
        {
            if (Time.timeScale == 1) // if the time scale is set to 1 and the game is not paused...
            {
                Time.timeScale = 0; // pause the game
                Screen.SetActive(true);
            }



            else // if anything else is the case...
            {
                Time.timeScale = 1; // resume the game
                Screen.SetActive(false);
            }
        }


    }
}
