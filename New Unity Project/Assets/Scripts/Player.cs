﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float moveSpeed;  // allows the movement speed of the player to be set in inspection window

    public Rigidbody2D rb;  // allows the rigidbody to be referenced
    public Camera cam; // allows the camera to be referenced

    Vector2 movement; // sets a vector 2 as equal to movement
    Vector2 mousePos; // sets a vector 2 as equal to mouse position

    // Update is called once per frame
    void Update()
    {
        // get the raw locations of both the x and y coordinates
        movement.x = Input.GetAxisRaw("Horizontal"); 
        movement.y = Input.GetAxisRaw("Vertical");

        mousePos = cam.ScreenToWorldPoint(Input.mousePosition); // turns the location of the cursor into an in-game point
    }

    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime); // allows the character to move by adding movement to the current position and multiplying it by speed and the amount of time passed

        // sets a variable to be equal to the angle between the mouse cursor and the player, then uses this angle to rotate the player
        Vector2 lookDir = mousePos - rb.position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        rb.rotation = angle;
    }
}
