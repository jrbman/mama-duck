﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Duckling : MonoBehaviour
{
    public Sprite newSprite;
    public SpriteRenderer spriteRenderer;


    void OnCollisionEnter2D(Collision2D other)
    {
        UnityEngine.Debug.Log("working");

        if (other.gameObject.tag == "pollution")
        {
            spriteRenderer.sprite = newSprite;
            this.gameObject.tag = "sick";

            if (GameObject.FindGameObjectsWithTag("sick").Length > 2) 
            {
                SceneManager.LoadScene(sceneName: "MainMenu");
            }
        }
    }
}
