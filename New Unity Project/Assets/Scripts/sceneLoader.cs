﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;  // imports the necessary scene tools

public class sceneLoader : MonoBehaviour
{
    
    public void LoadGame() // needs to be public so the button can access it
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // uses the scene manager to load the scene after the current scene
    }

    public void QuitGame()  // will activate when the quit button is pressed
    {
        Application.Quit(); // will exit the game once built
        Debug.Log("QUIT"); // just to see if it is working
    }


}
