﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pollution : MonoBehaviour
{

    public Vector2 aPosition1 = new Vector2(0, 0);

    void Update()
    {
        transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), aPosition1, 2.5f * Time.deltaTime);
    }
}
